#include <stdio.h>
#include <stdlib.h>

// nm cmake-build-debug/CMakeFiles/text-cxx.dir/text/uppercase.cxx.o | grep -i upper
// 0000000000000000 T _Z11toUpperCasePKc
extern const char* _Z11toUpperCasePKc(const char*);

int main() {
    const char* txt = "tjolla hopp from a tiny C program";
    const char* TXT = _Z11toUpperCasePKc(txt);

    printf("[C] %s --> %s\n", txt, TXT);
    free((void*) TXT);

    return 0;
}

#include <iostream>
#include <filesystem>
#include "outfile.hxx"

namespace rm = ribomation::io;
namespace fs = std::filesystem;
using namespace std::string_literals;
using std::cout;

int main() {
    auto filename = fs::path{"./dummy-file.txt"s};

    {
        auto f = rm::OutFile{filename};
        f.print("Hello from a tiny C++ object"s); f.print("\n");
        f.println("This is the 2nd line"s);
        f << "Finally, the last line is here"s << "\n";
    }

    auto numBytes = fs::file_size(filename);
    cout << "written " << numBytes << " bytes to " << filename << "\n";
}


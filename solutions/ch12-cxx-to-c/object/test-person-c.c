#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// [1] define a struct type (we don't need any content)
struct Person;

// [2] find the linker names
// nm `find . -name '*person*.o'` | grep -i person
// ./cmake-build-debug/CMakeFiles/object-cxx.dir/object/person.cxx.o:
// 00000000000001e6 T _Z9newPersonPKcj          - newPerson(char const*, unsigned)
// 0000000000000000 T _ZN6PersonC1EPKcj         - Person::Person(const char*, unsigned)
// 0000000000000000 T _ZN6PersonC1Ev            - Person::Person()
// 0000000000000138 T _ZN6PersonD1Ev            - Person::~Person()
// 0000000000000192 T _ZNK6Person7getNameEv     - Person::getName
// 00000000000001b0 T _ZNK6Person6getAgeEv      - Person::getAge
// 0000000000000206 T _ZN6Person7incrAgeEv      - Person::incrAge
// 00000000000002b8 T _ZN6Person6setAgeEj       - Person::setAge

// [3] define extern decls
extern struct Person* _Z9newPersonPKcj(const char*, unsigned);
extern struct Person* _ZN6PersonC1EPKcj(void*, const char*, unsigned);
extern struct Person* _ZN6PersonC1Ev(void*);
extern void*          _ZN6PersonD1Ev(struct Person*);
extern const char*    _ZNK6Person7getNameEv(struct Person*);
extern unsigned       _ZNK6Person6getAgeEv(struct Person*);
extern unsigned       _ZN6Person7incrAgeEv(struct Person*);
extern unsigned       _ZN6Person6setAgeEj(struct Person*, unsigned);


void usecase_cxx_factory() {
    printf("--- Use case C++ factory ---\n");
    struct Person* p = _Z9newPersonPKcj(strdup("Justin Time"), 25);
    printf("[C] Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p));

    _ZN6Person7incrAgeEv(p);
    printf("[C] Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p));

    _ZN6PersonD1Ev(p);
    free((void*) p);
}

void usecase_calloc() {
    printf("--- Use case calloc--\n");
    const size_t sizeOfPerson = 40;
    struct Person* p = (struct Person*)calloc(1, sizeOfPerson);
    _ZN6PersonC1EPKcj(p, strdup("Nisse Hult"), 37);
    printf("[C] Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p));

    _ZN6Person7incrAgeEv(p);
    printf("[C] Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p));

    _ZN6PersonD1Ev(p);
    free((void*) p);
}

void usecase_array() {
    printf("--- Use case array--\n");
    const int numPersons = 5;
    const size_t sizeOfPerson = 40;
    unsigned char storage[numPersons * sizeOfPerson];

    for (int k=0; k<numPersons; ++k) {
        void* ptr = storage + k * sizeOfPerson;
        _ZN6PersonC1Ev(ptr);
    }

    for (int k=0; k<numPersons; ++k) {
        void* ptr = storage + k * sizeOfPerson;
        _ZN6Person6setAgeEj(ptr, k * 5 + 20);
        printf("[C] Person{%s, %d}\n", _ZNK6Person7getNameEv(ptr), _ZNK6Person6getAgeEv(ptr));
    }

    for (int k=0; k<numPersons; ++k) {
        void* ptr = storage + k * sizeOfPerson;
        _ZN6PersonD1Ev(ptr);
    }
}

int main() {
    printf("Welcome to my nightmare in C... [A Cooper]\n");
    usecase_cxx_factory();
    usecase_calloc();
    usecase_array();
}

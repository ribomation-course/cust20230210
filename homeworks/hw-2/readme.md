# Homework 2

Design a data-type for handling a file containing equally sized data blocks

    BlockFile<Account>  db{"accounts.db"s};

The block data-type should only contain special member types
handling serialization/deserialization and specify a storage
capacity as a template parameter.

    Text<10>  text_value;
    Number<5> integral_value;

# Use-cases
Focus on one use-case at a time. Think, write, compile and run. Repeat.

## Sample block data-type

    struct Account {
      Text<10>  accno;
      Number<5> balance;
      //...
      friend ostream& operator<<(ostream& os, const Account& a);
      void operator +=(int amount);
    };

## Create an empty file and append blocks to it

    auto  db = BlockFile<Account>{"accounts.db"s};
    db << Account{"SEB12345"s, 100};

## Open an existing file and traverse all blocks using a for-each loop

    auto  db = BlockFile<Account>{"accounts.db"s};
    for (auto acc : db) cout << acc << endl;

## Index into a file for reading and for writing

    auto  db = BlockFile<Account>{"accounts.db"s};
    auto acc = db[3];
    acc += 42;
    db[3] = acc;


# Simplifying assumptions
* The file content and hence a block content is plain text, i.e. a byte is an ASCII char
* Block member data-types are text and integer only, with a defined storage size
* No need to make it feature complete; this is an exploratory assignment, i.e., for you to learn

# Block Member Types

Define two block member types, were N is their storage size in bytes/chars

    Text<N>
    Number<N>

Each type should have one member only, where the data payload is stored

    char storage[N];

* If new payload is smaller than *N*, pad the remaining storage with a padding char, such as `' '`, `'#'` or `'0'`
* If new payload is larger than *N*, truncate it to size *N*

Each type need conversion methods to and from the storage format

    // text
    void   value(string v)
    string value() const
    
    // number
    void value(int v)
    int  value() const 

Study the `std::string` class, investigate constructors and methods, such as `copy()`
* https://en.cppreference.com/w/cpp/string/basic_string

*In addition,*
you might want to add wrapper operators: 

    operator =(…)
    operator T()

Ensure you have simple unit tests using either of
* The [assert macro](https://en.cppreference.com/w/cpp/error/assert) 
* A proper unit-testing framework such as the header-only framework
[Catch2](https://github.com/catchorg/Catch2).

## Sample block content
Given the following block type

    struct Account {
      Text<10>  accno;
      Number<5> balance;
      //...
    };

and the action code

    BlockFile<Account>  db{"accounts.db"s};
    db << Account{"12345", 125};
    db << Account{"8888", 4321};

then, the file content should be

    12345#####001258888######04321

In this example we have chosen to:
* Let `Text` be left-adjusted and padded with `#` (*hash*)
* Let `Number` be right-adjusted and padded with `0` (*zero*).

# Hints for BlockFile\<T>

Study the `std::fstream` class by writing a few test programs
* https://en.cppreference.com/w/cpp/io/basic_fstream

Design the file class as a template class

    BlockFile<Block>

Start with rudimentary functionality and implement (private) methods for
* Write a block to the end of the file: `append(Block b)`
* Overwrite a block at specific position: `write(unsigned ix, Block b)`
* Read a block from a specific position: `read(unsigned ix, Block& b)`

Work in small increments
* Create an empty file and append 2-3 blocks, then inspect the file content
* Open the file and traverse it using a for-each loop and print each block
* Update a block using `operator[]` for read/write and inspect the file content


# Code Samples

## Start code for Text\<N>

    template<unsigned N>
    class Text {
        char    storage[N];
    public:
        Text() { value(""); }
        Text(string val) {  value(val); }

        void value(string val) {
            fill_n(storage, N, '#');
            val.copy(storage, N);
        }

        string value() const {
            auto str = string{begin(storage), end(storage)};
            auto pos = str.find_first_of('#');
            if (pos == string::npos) return str;
            return str.substr(0, pos);
        }
        //...
    };

## Value setter for Number\<N>

    void value(int val) {
        auto buf = ostringstream{};
        auto W   = N;
        if (val < 0) {
            buf << '-';
            --W;
        }
        buf << setfill('0') << setw(W) << abs(val);
        buf.str().copy(storage, N);
    }

## Start code for BlockFile\<T>

    template<typename Block>
    class BlockFile {
        fs::path dbpath;
        fstream file;

        void append(Block b) {
            file.seekp(0, ios::end);
            file.write(reinterpret_cast<const char*>(&b), sizeof(Block));
        }

        void write(unsigned ix, Block b) {
            ...
        }

        void read(unsigned ix, Block& b) {
            ...
        }

    public:
        explicit BlockFile(fs::path dbfile): dbpath{dbfile} {
            if (!fs::exists(dbpath)) {
                auto tmp = ofstream{dbfile}; //create empty dbfile
            }

            file.open(dbfile, ios::out | ios::in | ios::binary);
            if (file.fail()) {
                throw invalid_argument{"cannot open db-file: "s + dbfile.string()};
            }
        }
        ...

        unsigned size() const { ... }
        auto operator <<(Block b) -> BlockFile<Block>& { ... }

        struct Iterator {
            BlockFile<Block>&   db;
            unsigned            ix;
            ...
            void operator ++() { ... }
            Block operator *() { ... }
            operator Block() { ... }
            void operator =(Block b) { ... }
        };

        Iterator begin() { return {*this, 0}; }
        Iterator end()   { return {*this, size()}; }
        Iterator operator [](unsigned ix) { return {*this, ix}; }
    };

## Generator
Random values

    auto r = random_device{};

    auto nextAccno() -> string {
        auto nextLetter = uniform_int_distribution{'A', 'Z'};
        auto nextDigit  = uniform_int_distribution{'0', '9'};
        return ""s
            + nextLetter(r)
            + nextLetter(r)
            + nextLetter(r)
            + nextDigit(r)
            + nextDigit(r)
            + nextDigit(r);
    }

    auto nextBalance() -> int {
        auto nextValue = normal_distribution<float>{100, 75};
        return nextValue(r);
    }

Inflating a file

    if (fs::exists(file)) fs::remove(file);

    auto db = BlockFile<Account>{file};
    for (auto k  = 0; k < N; ++k)
        db << Account{nextAccno(), nextBalance()};

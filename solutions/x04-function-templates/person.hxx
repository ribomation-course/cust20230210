#pragma once
#include <string>

struct Person {
    const std::string name;
    Person(std::string n) : name{n} {}
};

inline auto operator==(const Person& lhs, const Person& rhs) -> bool {
    return lhs.name == rhs.name;
}

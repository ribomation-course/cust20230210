#include <iostream>
#include <filesystem>
#include <string>
#include <unordered_set>
#include "count.hxx"

using namespace std::string_literals;
namespace rm = ribomation::io;
namespace fs = std::filesystem;
using std::cout;
using std::endl;
using std::string;

bool isTextFile(const fs::path& p) {
    static const auto TEXT_EXTS = std::unordered_set<string>{
            ".txt", ".cxx", ".cpp", ".hxx", ".log",
            ".h", ".c", ".cmake", ".make"
    };
    return fs::is_regular_file(p) && TEXT_EXTS.count(p.extension().string()) > 0;
}

int main(int argc, char** argv) {
    auto dir = fs::path{argc == 1 ? ".." : argv[1]};
    if (!fs::is_directory(dir)) { dir = fs::current_path(); }
    cout << "Dir: " << fs::canonical(dir) << endl;

    auto total = rm::Count{"TOTAL"s};
    for (const auto& e: fs::recursive_directory_iterator{dir}) {
        const auto& p = e.path();
        if (isTextFile(p)) {
            auto cnt = rm::Count{p};
            total += cnt;
            cout << cnt << endl;
        }
    }
    cout << total << endl;
}

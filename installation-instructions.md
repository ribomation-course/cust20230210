# Installation Instructions

To perform the programming exercises, you need:
* Modern C++ compiler, supporting at least C++17 but preferable C++20, _to compile your programs_
* Decent C++ IDE, _to write your code_
* GIT client, _to easily get the solutions from this repo_


# Installation of Ubuntu Linux
We will do the exercises on Linux. Therefore, you need access to a Linux or Unix system, preferably Ubuntu.
Read our common guide of how to install Linux on WSL and a C++ compiler.

* [Linux and C++ Installation](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)


# Installation within Ubuntu Linux
## The GNU C/C++ compiler version 12 and builder tools

    sudo apt install make cmake g++-12 gdb valgrind

_N.B._, If you cannot get version 12, try version 11, _i.e._, `g++-11`  

## Additional tools

    sudo apt install tree git


_N.B._ when you run a `sudo` command it prompts you for the password, you use
to log on to Ubuntu. If you're running another OS, amend the installation command
accordingly.



#include <iostream>
#include <sstream>
#include "vector-3d.hxx"

using std::cout;
using std::endl;
using std::istringstream;
namespace rm = ribomation::types;

void usecase1() {
    cout << "-- Vector3d<int> --\n";
    
    auto v1 = rm::Vector3d<>{2, 5, 10};
    cout << "v1: " << v1 << endl;
    cout << "sizeof: " << sizeof(v1) << endl;

    istringstream{"data: 10 20 30"} >> v1;
    cout << "v1: " << v1 << endl;

    cout << "v1 * 42: " << v1 * 42 << endl;
    cout << "10 * v1: " << 10 * v1 << endl;

    auto v2 = rm::Vector3d<>{2, 2, 2};
    cout << "v2: " << v2 << endl;

    cout << "v1 + v2: " << v1 + v2 << endl;
    cout << "v1 - v2: " << v1 - v2 << endl;
    cout << "v1 * v2: " << v1 * v2 << endl;
}

void usecase2() {
    cout << "-- Vector3d<long double> --\n";
    
    auto v1 = rm::Vector3d<long double>{2, 5, 10};
    cout << "v1: " << v1 << endl;
    cout << "sizeof: " << sizeof(v1) << endl;

    auto v2 = rm::Vector3d<long double>{2, 2, 2};
    cout << "v2: " << v2 << endl;

    cout << "v1 + v2: " << v1 + v2 << endl;
    cout << "v1 - v2: " << v1 - v2 << endl;
    cout << "v1 * v2: " << v1 * v2 << endl;
}

int main() {
    usecase1();
    usecase2();
}

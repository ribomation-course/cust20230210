#pragma once
#include <iostream>
#include <cctype>

namespace ribomation::types {
    using std::ostream;
    using std::istream;

    template<typename T = int>
    class Vector3d {
        T x = {};
        T y = {};
        T z = {};

    public:
        Vector3d(T x, T y, T z) : x{x}, y{y}, z{z} {}

        T getX() const { return x; }
        T getY() const { return y; }
        T getZ() const { return z; }
    };

    template<typename T>
    auto operator<<(ostream& os, const Vector3d<T>& v) -> ostream& {
        return os << "[" << v.getX() << ", " << v.getY() << ", " << v.getZ() << "]";
    }

    template<typename T>
    auto operator>>(istream& is, Vector3d<T>& vec) -> istream& {
        char ch;
        while (is.get(ch) && !isdigit(ch)); //eat non-digits
        is.putback(ch); //put back first found digit

        T x, y, z;
        is >> x >> y >> z;
        vec = Vector3d{x, y, z};

        return is;
    }

    template<typename T>
    auto operator*(const Vector3d<T>& v, T factor) -> Vector3d<T> {
        return {v.getX() * factor,
                v.getY() * factor,
                v.getZ() * factor};
    }

    template<typename T>
    auto operator*(T factor, const Vector3d<T>& v) -> Vector3d<T> {
        return v * factor;
    }

    template<typename T>
    auto operator+(const Vector3d<T>& lhs, const Vector3d<T>& rhs) -> Vector3d<T> {
        return {lhs.getX() + rhs.getX(),
                lhs.getY() + rhs.getY(),
                lhs.getZ() + rhs.getZ()};
    }

    template<typename T>
    auto operator-(const Vector3d<T>& v) -> Vector3d<T> {
        return {-v.getX(), -v.getY(), -v.getZ()};
    }

    template<typename T>
    auto operator-(const Vector3d<T>& lhs, const Vector3d<T>& rhs) -> Vector3d<T> {
        return lhs + -rhs;
    }

    template<typename T>
    auto operator*(const Vector3d<T>& lhs, const Vector3d<T>& rhs) -> T {
        return lhs.getX() * rhs.getX()
               + lhs.getY() * rhs.getY()
               + lhs.getZ() * rhs.getZ();
    }

}

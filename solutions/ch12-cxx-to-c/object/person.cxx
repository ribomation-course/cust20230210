#include <iostream>
#include "person.hxx"

using namespace std;

Person::Person() : name{}, age{0} {
    cout << "+Person{} @ " << this << endl;
}

Person::Person(const char* name_, unsigned age_)
        : name{name_}, age{age_} {
    cout << "+Person{" << name << ", " << age << "} @ " << this << endl;
}

Person::~Person() {
    cout << "~Person{"<< name << "} @ " << this << endl;
}

auto Person::getName() const -> const char* {
    return name.c_str();
}

unsigned Person::getAge() const {
    return age;
}

unsigned Person::incrAge() {
    return ++age;
}

unsigned Person::setAge(unsigned int a) {
    age = a;
    return age;
}

auto newPerson(const char* name_, unsigned age_) -> Person* {
    return new Person(name_, age_);
}

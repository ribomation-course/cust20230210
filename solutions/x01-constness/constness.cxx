#include <iostream>
#include <string>
#include <utility>

using namespace std::string_literals;
using std::cout;
using std::string;

class Person {
    string name;
    unsigned age;
public:
    Person(string name, unsigned int age)
            : name(std::move(name)), age(age) {}

    void incrAge() { ++age; }

    auto getAge() const { return age; }
};

void const_context(Person const& q) {
    cout << "(1) q.age: " << q.getAge() << "\n";
    const_cast<Person&>(q).incrAge();
    cout << "(2) q.age: " << q.getAge() << "\n";
}

int main() {
    auto p = Person{"Nisse"s, 42};
    const_context(p);
    cout << "(3) p.age: " << p.getAge() << "\n";
}

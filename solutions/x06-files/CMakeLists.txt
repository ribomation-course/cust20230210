cmake_minimum_required(VERSION 3.22)
project(x06_files)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(dir-count count.hxx dir-count.cxx)
target_compile_options(dir-count PRIVATE ${WARN})



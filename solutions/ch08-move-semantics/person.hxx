#pragma once

#include <iostream>
#include <sstream>
#include <cstring>

namespace ribomation::domain {
    using std::cout;

    inline auto strClone(char const* str) -> char* {  //strdup() is using malloc()
        if (str == nullptr) return nullptr;
        return ::strcpy(new char[::strlen(str) + 1], str);
    }

    class Person {
        char* name = nullptr;
        unsigned age = 0;

    public:
        ~Person() {
            cout << "~Person @ " << this << "\n";
            delete[] name;
        }

        Person() {
            cout << "Person{} @ " << this << "\n";
            name = strClone("");
        }

        Person(const char* n, unsigned a) : name{strClone(n)}, age{a} {
            cout << "Person{const char* " << n << ", " << a << "} @ " << this << "\n";
        }

        Person(Person const& that) = delete;
        Person& operator=(Person const& rhs) noexcept = delete;
        
        Person(Person&& that) noexcept: name{that.name}, age{that.age} {
            that.name = nullptr;
            that.age  = 0;
            cout << "Person{Person&& " << &that << "} @ " << this << "\n";
        }

        Person& operator=(Person&& rhs) noexcept {
            if (this != &rhs) {
                delete[] name;
                name = rhs.name;
                rhs.name = nullptr;
                age = rhs.age;
                rhs.age = 0;
            }
            cout << "operator={Person&& " << &rhs << "} @ " << this << "\n";
            return *this;
        }

        auto incrAge() {
            return ++age;
        }

        [[nodiscard]] auto toString() const -> std::string {
            auto buf = std::ostringstream{};
            buf << "Person(" << (name ? name : "??") << ", " << age << ") @ " << this;
            return buf.str();
        }

        friend auto operator <<(std::ostream& os, Person const& p) -> std::ostream& {
            return os << p.toString();
        }
    };

}


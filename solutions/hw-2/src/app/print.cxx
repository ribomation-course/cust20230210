#include <iostream>
#include <string>
#include <locale>
#include "block-file.hxx"
#include "account.hxx"

using namespace std;
using namespace std::string_literals;
using namespace ribomation;
using namespace ribomation::bank;


int main(int argc, char** argv) {
    auto const file = "account.db"s;

    auto      total = 0;
    auto      db    = BlockFile<Account>{file};
    for (auto acc: db) {
        cout << acc << endl;
        total += acc;  // acc.operator int()
    }
    cout.imbue(locale("en_US.UTF-8"));
    cout << "total balance: SEK " << total << endl;

    return 0;
}

#include <iostream>
#include <string>
#include <functional>
#include "stack.hxx"

using namespace std::string_literals;
using std::string;
using std::to_string;
using std::cout;
using std::endl;
using std::ostream;
using std::function;


struct Person {
    string name;
    Person() = default;
    Person(string n) : name{std::move(n)} {}
    friend auto operator <<(ostream& os, const Person& p) -> ostream& {
        return os << "Person(" << p.name << ") @ " << &p;
    }
};

template<typename T, typename Collection>
void populate(Collection& coll, function<T (int)> produce) {
    for (int k = 1; !coll.full(); ++k) coll.push(produce(k));
}

template<typename Collection>
void drain(Collection& coll) {
    while (!coll.empty()) cout << coll.pop() << endl;
}

void usecase_int() {
    cout << "--- Stack<int, 5> -------\n";
    auto s = Stack<int, 5>{};
    cout << "sizeof(int) = " << sizeof(int) << endl;
    cout << "sizeof(Stack<int, 5>) = " << sizeof(s) << endl;

    populate<int>(s, [](int k) -> int {return k*10;});
    drain(s);
}

void usecase_string() {
    cout << "--- Stack<string, 5> -------\n";
    auto s = Stack<string, 5>{};
    cout << "sizeof(string) = " << sizeof(string) << endl;
    cout << "sizeof(Stack<string, 5>) = " << sizeof(s) << endl;

    populate<string>(s, [](int k){return "nisse-"s + to_string(k);});
    drain(s);
}

void usecase_object() {
    cout << "--- Stack<Person, 5> -------\n";
    auto s = Stack<Person, 5>{};
    cout << "sizeof(Person) = " << sizeof(Person) << endl;
    cout << "sizeof(Stack<Person, 5>) = " << sizeof(s) << endl;

    populate<Person>(s, [](int k) {return Person{"anna-"s + to_string(k)};});
    drain(s);
}

int main() {
    usecase_int();
    usecase_string();
    usecase_object();
}

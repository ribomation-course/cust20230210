cmake_minimum_required(VERSION 3.16)
project(homework_1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_executable(word-cloud
        src/word-cloud.cxx
        )

#include <iostream>
#include <string>
#include "block-file.hxx"
#include "account.hxx"

using namespace std;
using namespace std::string_literals;
using namespace ribomation;
using namespace ribomation::bank;


int main(int argc, char** argv) {
    auto const file = "account.db"s;

    auto      total = 0;
    auto      db    = BlockFile<Account>{file};
    for (auto ix    = 0U; ix < db.size(); ++ix) {
        Account acc = db[ix];  // db[ix].operator Account()
        acc += 100;
        db[ix] = acc;  //db[ix].operator =( acc )
        total += acc;
    }
    cout.imbue(locale("en_US.UTF-8"));
    cout << "updated total balance: SEK " << total << endl;

    return 0;
}

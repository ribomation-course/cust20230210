#pragma once
#include <string>
#include <fstream>
#include <stdexcept>
#include <filesystem>

namespace ribomation {
    using namespace std;
    using namespace std::literals;
    namespace fs = std::filesystem;

    template<typename Block>
    class BlockFile {
        fs::path dbpath;
        fstream file;

        void append(Block b) {
            file.seekp(0, ios::end);
            file.write(reinterpret_cast<const char*>(&b), sizeof(Block));
        }

        void write(unsigned ix, Block b) {
            file.seekp(ix * sizeof(Block), ios::beg);
            file.write(reinterpret_cast<const char*>(&b), sizeof(Block));
        }

        void read(unsigned ix, Block& b) {
            file.seekg(ix * sizeof(Block), ios::beg);
            file.read(reinterpret_cast<char*>(&b), sizeof(Block));
        }

    public:
        explicit BlockFile(fs::path dbfile): dbpath{dbfile} {
            if (!fs::exists(dbpath)) {
                auto tmp = ofstream{dbfile}; //touch dbfile
            }

            file.open(dbfile, ios::out | ios::in | ios::binary);
            if (file.fail()) {
                throw invalid_argument{"cannot open db-file: "s + dbfile.string()};
            }
        }

        ~BlockFile() {
            file.flush();
            file.sync();
            file.close();
        }

        unsigned size() const {
            return fs::file_size(dbpath) / sizeof(Block);
        }

        auto operator <<(Block b) -> BlockFile<Block>& {
            append(b);
            return *this;
        }

        struct Iterator {
            BlockFile<Block>&   db;
            unsigned            ix;

            Iterator(BlockFile<Block>& db, unsigned int ix) : db(db), ix(ix) {}

            bool operator !=(Iterator that) {
                return this->ix != that.ix;
            }

            void operator ++() {
                ++ix;
            }

            Block operator *() {
                Block b;
                db.read(ix, b);
                return b;
            }

            operator Block() {
                return operator *();
            }

            void operator =(Block b) {
                db.write(ix, b);
            }
        };

        Iterator begin() { return {*this, 0}; }

        Iterator end() { return {*this, size()}; }

        Iterator operator [](unsigned ix) { return {*this, ix}; }

    };

}

#include <iostream>
#include <string>

#define extends
#define implements
#define interface struct
#define abstract 0

using namespace std::string_literals;
using std::cout;
using std::endl;
using std::string;


struct Object {
    virtual ~Object() = default;
};

interface Stringable {
    virtual string toString() const = abstract;
};

template<typename T>
interface Comparable {
    virtual int compareTo(const T& rhs) const = abstract;
};


struct Person :
        extends Object,
        implements Stringable, Comparable<Person>
{
    using super = Object;

    explicit Person(string name_) : super(), name{std::move(name_)} {
        cout << "Person{" << name << "} @ " << this << endl;
    }

    ~Person() override {
        cout << "~Person{" << name << "} @ " << this << endl;
    }

    string toString() const override {
        return "Person{" + name + "}"s;
    }

    int compareTo(const Person& rhs) const override {
        return name.compare(rhs.name);
    }

private:
    const string name;
};


void use(Object* obj) {
    if (auto p = dynamic_cast<Person*>(obj); p != nullptr) {
        cout << "[use] p: " << p->toString() << endl;

        Person* q = new Person{"Nisse"};
        if (p->compareTo(*q) == 0) cout << "[use] p == q\n";
        delete q;
    }
}

int main() {
    Object* obj = new Person{"Nisse"};
    use(obj);
    delete obj;
    return 0;
}


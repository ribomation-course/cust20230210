#include <iostream>
#include <tuple>
#include <map>

using std::cout;

auto fib(int n) {
    if (n == 0) return 0UL;
    if (n == 1) return 1UL;
    return fib(n - 2) + fib(n - 1);
}

auto compute(int n) {
    return std::make_tuple(n, fib(n));
}

auto table(int n) {
    auto m = std::map<int, unsigned long>{};
    for (auto k = 1; k <= n; ++k) {
        auto [a, f] = compute(k);
        m[a] = f;
    }
    return m;
}

int main() {
    auto const N = 45;
    {
        auto result = fib(N);
        cout << "fib(" << N << ") = " << result << "\n";
    }
    {
        auto [n, f] = compute(N);
        cout << "fib(" << n << ") = " << f << "\n";
    }
    {
        for (auto [n, f]: table(N))
            cout << "fib(" << n << ") = " << f << "\n";
    }
}

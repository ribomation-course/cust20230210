# Homework 1

Write a C++ program that:
* Opens a text file, were its name is provided on the command-line
* Extracts all words (keep just the letters)
* Aggregates the word frequencies (word size >= W)
* Sorts them in descending frequency order
* Keeps the N most frequent words
* Converts each word-frequency pair into a HTML <span> tag, with an inline style setting the size proportional to the frequency
* Writes out a complete HTML file with the words, to a file named as the input file but with the extension replaced by .html
* Finally, it should print out
    - Input file size
    - Number of words collected
    - Elapsed time in seconds

# Text Files

* Small text file, ~1.2MB: [musketeers.txt](./musketeers.txt)
* Small text file, ~5.3MB: [shakespeare.txt](./shakespeare.txt)
* Collection of large text files [English Texts](http://pizzachili.dcc.uchile.cl/texts/nlang/)

*N.B.* The large files are GZIP compressed. Uncompress them before use with the command

    gunzip english.50MB.gz

# Hints

## Command-line arguments processing

    auto filename = "../files/musketeers.txt"s;
    auto maxWords = 100U;
    auto minSize  = 5U;
    auto outdir   = "./"s;
    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-f"s) {
            filename = argv[++k];
        } else if (arg == "-d"s) {
            outdir = argv[++k];
        } else if (arg == "-w"s) {
            maxWords = stoi(argv[++k]);
        } else if (arg == "-s"s) {
            minSize = stoi(argv[++k]);
        } else {
            cerr << "unknown option: " << arg << endl;
            cerr << "usage: " << argv[0] << " [-f <file>] [-w <max-words>] [-s <min-size>]" << endl;
            return 1;
        }
    }

## Helpful type definitions

    using WordFreqs = unordered_map<string, unsigned>;
    using WordFreq  = pair<string, unsigned>;
    using Pairs     = vector<WordFreq>;
    using Strings   = vector<string>;

By expressing input and output types of the functions in terms of the 
types above, it helps focus on the application problem. 

For example, here is a list of function declarations for the whole
problem, that sub-divides into smaller sub-problems.

    auto load(const string& filename, unsigned minSize) -> WordFreqs;
    auto strip(string line) -> Strings;
    auto mostFrequent(const WordFreqs& freqs, unsigned maxWords) -> Pairs;
    auto asTags(Pairs pairs) -> Strings;
    auto store(const Strings& tags, const string& infile, const string& outdir) -> string;

Another example, is sorting a sequence of word-frequency pairs

    sort(begin(sortable), end(sortable), [](const WordFreq& lhs, const WordFreq& rhs) {...})

## Read line-by-line

    for (string line; getline(in, line);) { ... }

## Read word-by-word

    for (string word; in >> word;) { ... }

## Normalized font-size

    auto maxFreq     = pairs.front().second;
    auto minFreq     = pairs.back().second;
    auto maxFontSize = 150.0;
    auto minFontSize = 15.0;
    auto scale       = (maxFontSize - minFontSize) / (maxFreq - minFreq);
    ...
    auto normalizedFreq = static_cast<int>(scale * freq );

## How to generate a random HTML color

    auto randColor() -> string {
        auto val = uniform_int_distribution{0, 255};
        auto HEX = [&val]() {
            auto buf = ostringstream{};
            buf << setw(2) << setfill('0') << hex << uppercase << val(r);
            return buf.str();
        };
        return "#"s + HEX() + HEX() + HEX();
    }

Colorize all words, was not part of the assignment text. However, it's fun to do anyway.

    <span style="font-size: FREQpx; color: COLOR;">WORD</span>

## HTML prefix

    <html>
      <head>
        <title>Word Cloud</title>
      </head>
      <body>
        <h1>Word Cloud</h1>

## HTML suffix

    </body>
    </html>



cmake_minimum_required(VERSION 3.22)
project(ch09_operator_overloading)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(vector-3d vector-3d.hxx app.cxx)
target_compile_options(vector-3d PRIVATE ${WARN})


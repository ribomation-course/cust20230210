#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <filesystem>
#include <stdexcept>
#include <string>
#include <cctype>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <random>
#include <chrono>
#include <tuple>

using namespace std::literals::string_literals; 
namespace fs = std::filesystem;
namespace ch = std::chrono;
using std::string;
using std::cout;
using std::endl;

using WordFreqs = std::unordered_map<string, unsigned>;
using WordFreq = std::pair<string, unsigned>;
using Pairs = std::vector<WordFreq>;
using Strings = std::vector<string>;

struct Parameters {
    string   filename = "../files/musketeers.txt"s;
    string   outdir   = "./"s;
    unsigned maxWords = 100U;
    unsigned minSize  = 5U;
};

auto parseArgs(int argc, char** argv) -> Parameters;
auto load(const string& filename, unsigned minSize) -> WordFreqs;
auto strip(string line) -> Strings;
auto mostFrequent(const WordFreqs& freqs, unsigned maxWords) -> Pairs;
auto asTags(Pairs pairs) -> Strings;
auto randColor(std::random_device& r) -> string;
auto store(const Strings& tags, const string& infile, const string& outdir) -> string;


int main(int argc, char** argv) {
    auto startTime = ch::high_resolution_clock::now();
    {
        auto params    = parseArgs(argc, argv);
        auto wordFreqs = load(params.filename, params.minSize);
        auto pairs     = mostFrequent(wordFreqs, params.maxWords);
        auto tags      = asTags(pairs);
        auto htmlFile  = store(tags, params.filename, params.outdir);
    }
    auto endTime   = ch::high_resolution_clock::now();
    cout << "Elapsed " << ch::duration_cast<ch::milliseconds>(endTime - startTime).count() << " ms" << endl;
}

auto load(const string& filename, unsigned minSize) -> WordFreqs {
    cout << "Loading " << filename << ", " << fs::file_size(filename) / (1024.0 * 1024) << " MB" << endl;

    auto file = std::ifstream{filename};
    if (!file) {
        throw std::invalid_argument{"cannot open "s + filename};
    }

    auto        freqs = WordFreqs{};
    for (string line; getline(file, line);) {
        auto words = strip(line);
        for (const auto& word: words) {
            if (word.size() >= minSize) ++freqs[word];
        }
    }

    cout << "Loaded " << freqs.size() << " words" << endl;
    return freqs;
}

auto mostFrequent(const WordFreqs& freqs, unsigned maxWords) -> Pairs {
    auto sortable   = Pairs{begin(freqs), end(freqs)};
    auto comparator = [](WordFreq const& lhs, WordFreq const& rhs) { return lhs.second > rhs.second; };

    std::partial_sort(begin(sortable), begin(sortable) + maxWords - 1, end(sortable), comparator);
    sortable.erase(begin(sortable) + maxWords, end(sortable));
    sortable.shrink_to_fit();

    return sortable;
}

auto asTags(Pairs pairs) -> Strings {
    auto maxFontSize = 150.0;
    auto minFontSize = 15.0;
    auto maxFreq     = pairs.front().second;
    auto minFreq     = pairs.back().second;
    auto scale       = (maxFontSize - minFontSize) / (maxFreq - minFreq);
    auto r           = std::random_device{}; // /dev/random

    auto tags = Strings{};
    tags.reserve(pairs.size());

    std::transform(begin(pairs), end(pairs), back_inserter(tags), [&r, scale](const WordFreq& wf) {
        auto word     = wf.first;
        auto freq     = wf.second;
        auto normFreq = static_cast<int>(scale * freq );
        auto buf      = std::ostringstream{};
        buf << "<span style='font-size:" << normFreq << "px;"
            << " color:" << randColor(r) << ";'>"
            << word
            << "</span>";
        return buf.str();
    });
    shuffle(begin(tags), end(tags), r);

    return tags;
}

auto store(const Strings& tags, const string& infile, const string& outdir) -> string {
    auto htmlFile = fs::path(outdir + fs::path(infile).stem().string() + ".html"s);
    auto file     = std::ofstream{htmlFile};
    if (!file) {
        throw std::runtime_error{"cannot open "s + htmlFile.string() + " for writing"s};
    }

    file << R"(<html>
    <head>
        <title>Word Cloud</title>
    </head>
    <body>
        <h1>Word Cloud</h1>
    )";
    for (const auto& tag: tags) file << tag << "\n";
    file << "</body></html>";

    cout << "Written " << htmlFile << endl;
    return htmlFile;
}

auto strip(string line) -> Strings {
    transform(begin(line), end(line), begin(line), [](auto ch) {
        return isalpha(ch) ? tolower(ch) : ' ';
    });
    auto        buf   = std::istringstream{line};
    auto        words = Strings{};
    for (string word; buf >> word;) words.push_back(word);
    return words;
}

auto randColor(std::random_device& r) -> string {
    using std::setw;
    using std::setfill;
    using std::hex;
    using std::uppercase;

    auto val = std::uniform_int_distribution{0, 255};
    auto HEX = [&]() {
        auto buf = std::ostringstream{};
        buf << setw(2) << setfill('0') << hex << uppercase << val(r);
        return buf.str();
    };
    return "#"s + HEX() + HEX() + HEX();
}

auto parseArgs(int argc, char** argv) -> Parameters {
    auto params = Parameters{};

    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-f"s) {
            params.filename = argv[++k];
        } else if (arg == "-d"s) {
            params.outdir = argv[++k];
        } else if (arg == "-w"s) {
            params.maxWords = std::stoi(argv[++k]);
        } else if (arg == "-s"s) {
            params.minSize = std::stoi(argv[++k]);
        } else {
            std::cerr << "unknown option: " << arg << endl;
            std::cerr << "usage: " << argv[0] << " [-f <file>] [-w <max-words>] [-s <min-size>]" << endl;
            throw std::invalid_argument{"unknown argument: "s + arg};
        }
    }

    return params;
}

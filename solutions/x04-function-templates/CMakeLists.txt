cmake_minimum_required(VERSION 3.22)
project(x04_function_templates)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(function-templates person.hxx equals.hxx equals-test.cxx)
target_compile_options(function-templates PRIVATE ${WARN})



#include <algorithm>
#include <cstring>
#include <cctype>

auto toUpperCase(const char* txt) -> const char* {
    auto const SIZE = strlen(txt);

    auto       buf  = new char[SIZE + 1];
    std::transform(txt, txt + SIZE, buf, [](auto ch) { return ::toupper(ch); });
    buf[SIZE] = '\0';

    return buf;
}

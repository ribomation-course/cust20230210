#include <iostream>
#include <functional>
#include <algorithm>
#include <string>

using std::cout;

void task_1() {
    cout << "-- task 1 --\n";
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);
    std::for_each(numbers, numbers + N, [](int x) { cout << x << " "; });
    cout << "\n";
    auto factor = 42;
    std::transform(numbers, numbers + N, numbers, [factor](int x) { return factor * x; });
    std::for_each(numbers, numbers + N, [](int x) { cout << x << " "; });
    cout << "\n";
}

void modify(int* arr, int size, std::function<int(int)> fn) {
    for (auto k = 0; k < size; ++k) arr[k] = fn(arr[k]);
}

void task_2() {
    cout << "-- task 2 --\n";
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);
    std::for_each(numbers, numbers + N, [](int x) { cout << x << " "; });
    cout << "\n";
    modify(numbers, N, [](int k) -> int { return k * k; });
    std::for_each(numbers, numbers + N, [](int x) { cout << x << " "; });
    cout << "\n";
}

auto count_if(int* first, int* last, std::function<bool(int)> pred) {
    auto cnt = 0U;
    for ( ; first != last; ++first) if (pred(*first)) ++cnt;
    return cnt;
}

void task_3() {
    cout << "-- task 3 --\n";
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);
    auto res = count_if(numbers, numbers + N, [](int x) { return x % 2 == 0; });
    cout << "res = " << res << "\n";
}

int main() {
    task_1();
    task_2();
    task_3();
}

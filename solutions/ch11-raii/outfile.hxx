#pragma once

#include <string>
#include <stdexcept>
#include <cstring>
#include <cerrno>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

namespace ribomation::io {
    using  std::string;
    using  std::runtime_error;
    using namespace std::string_literals;

    class OutFile {
        const int fd;

    public:
        explicit OutFile(const string& filename)
        : fd{ ::open(filename.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644) }
        {
            if (fd == -1) {
                throw runtime_error{"open failed: "s + strerror(errno)};
            }
        }

        ~OutFile() {
            ::close(fd);
        }

        void print(const string& str) const {
            unsigned long cnt = ::write(fd, str.c_str(), str.size());
            if (cnt != str.size()) {
                throw runtime_error{"print failed: "s + strerror(errno)};
            }
        }

        void println(const string& str) const {
            print(str + "\n"s);
        }
    };
    
    inline auto operator <<(OutFile& f, string const& txt) -> OutFile& {
        f.print(txt);
        return f;
    }

}

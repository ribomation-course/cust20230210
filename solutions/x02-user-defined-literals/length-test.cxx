#include <iostream>
#include <iomanip>
#include "length.hxx"
using std::cout;
using std::fixed;
using std::setprecision;
using namespace ribomation::length;

int main() {
    auto distance = 2_km + 12.5_m - 0.5_mi + 31_ya;
    cout << "distance: " << fixed << setprecision(3) << distance << "\n";
}

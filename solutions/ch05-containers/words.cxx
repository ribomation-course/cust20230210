#include <iostream>
#include <string>
#include <algorithm>
#include <set>
#include <cctype>
using std::cout;
using std::cin;
using std::string;

int main() {
    auto words = std::set<string>{};

    for (string characters; cin >> characters;) {
        string word{};
        std::copy_if(std::begin(characters), std::end(characters), std::back_inserter(word), [](char ch){
            return ::isalpha(ch);
        });
        if (word.size() > 2) words.insert(word);
    }

    for (auto const& w : words) cout << w << " ";
    cout << "\n";
}

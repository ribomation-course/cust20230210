cmake_minimum_required(VERSION 3.22)
project(ch04_lambda)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(lambda lambda.cxx)
target_compile_options(lambda PRIVATE ${WARN})



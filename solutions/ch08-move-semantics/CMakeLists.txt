cmake_minimum_required(VERSION 3.22)
project(ch08_move_semantics)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(person-app person.hxx person-app.cxx)
target_compile_options(person-app PRIVATE ${WARN})



#pragma once

#include <string>

class Person {
    std::string  name;
    unsigned     age;
public:
    Person();
    Person(const char* name, unsigned age);
    ~Person();
    const char* getName() const;
    unsigned getAge() const;
    unsigned incrAge();
    unsigned setAge(unsigned);
};

extern auto newPerson(const char* name, unsigned age) -> Person*;

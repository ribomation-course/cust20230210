#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::string;
using namespace std::string_literals;

int main() {
    std::vector<string> words = {
            "hi"s, "hello"s, "hepp"s, "tjabba"s
    };
    for (auto const& w: words) cout << w << " ";
    cout << "\n";

    for (std::vector<std::string>::iterator it = words.begin(); it != words.end(); ++it) cout << w << " ";

    string sentence = "there was a dark and silent night, suddenly there was a shot."s;

    if (unsigned long idx = sentence.find("silent"s); idx != string::npos) {
        cout << "found: " << sentence.substr(idx, 6) << "\n";
    }
    if (unsigned long idx = sentence.find("shot"s); idx != string::npos) {
        cout << "found: " << sentence.substr(idx, 4) << "\n";
    }
    if (auto idx = sentence.find("xyz"s); idx != string::npos) {
        cout << "found: " << sentence.substr(idx, 4) << "\n";
    } else {
        cout << "not found\n";
    };
}

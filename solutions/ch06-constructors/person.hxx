#pragma once
#include <string>
#include <iostream>

namespace ribomation::domain {
    using std::string;

    class Person {
        string name{};
        unsigned age{};
        inline static int count = 0;
    public:
        Person() = default;
        Person(string name_, unsigned age_) : name{std::move(name_)}, age{age_} {
            ++count;
        }
        Person(Person const& rhs) : name{rhs.name}, age{rhs.age} {
            ++count;
        }
        Person(Person&& rhs) noexcept : name{std::move(rhs.name)}, age{rhs.age} {
            ++count;
        }
        ~Person() {
            --count;
        }

        friend auto operator <<(std::ostream& os, Person const& p) -> std::ostream& {
            return os << "Person{" << p.name << ", " << p.age << "} @ " << &p;
        }

        static auto instances() { return count; }
    };

}

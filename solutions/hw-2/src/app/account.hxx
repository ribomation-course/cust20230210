#pragma once
#include <iosfwd>
#include "text.hxx"
#include "number.hxx"

namespace ribomation::bank {

    class Account {
        Text<10>  accno;
        Number<6> balance;
    public:
        Account() = default;
        ~Account() = default;
        Account(const Account&) = default;
        Account(string accno) : accno{accno} {}
        Account(string accno, int balance) : accno{accno}, balance{balance} {}

        void operator +=(int amount) {
            balance = balance + amount;
        }

        operator string() const { return accno; }
        operator int() const { return balance; }

        friend auto operator <<(ostream& os, const Account& obj) -> ostream& {
            return os << "Account{" << obj.accno << ", SEK " << obj.balance << "}";
        }
    };

     
}


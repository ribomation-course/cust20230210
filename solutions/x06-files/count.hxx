#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <filesystem>
#include <string>
#include <utility>

namespace ribomation::io {
    using namespace std::string_literals;
    namespace fs = std::filesystem;
    using std::string;

    struct Count {
        const string name;
        unsigned lines = 0;
        unsigned words = 0;
        unsigned chars = 0;

        explicit Count(string name) : name{std::move(name)} {}
        explicit Count(const fs::path& file) : name{file.string()} {
            update(file);
        }

        void update(const fs::path& file) {
            chars += fs::file_size(file);
            auto f = std::ifstream{file};
            for (string line; std::getline(f, line);) {
                ++lines;
                auto buf = std::istringstream{line};
                for (string word; buf >> word;) ++words;
            }
        }

        void update(const Count& cnt) {
            lines += cnt.lines;
            words += cnt.words;
            chars += cnt.chars;
        }

        void operator +=(const Count& cnt) { update(cnt); }

        friend auto operator <<(std::ostream& os, const Count& cnt) -> std::ostream& {
            using std::setw;
            return os << setw(6) << cnt.lines
                      << setw(8) << cnt.words
                      << setw(12) << cnt.chars
                      << " " << cnt.name;
        }

    };
}

#include <iostream>
#include <vector>
#include "person.hxx"

namespace rm = ribomation::domain;
using namespace std::string_literals;
using std::cout;
using std::string;

void use_case_1() {
    auto anna = rm::Person{"Anna"s, 22};
    auto berit = rm::Person{"Berit"s, 32};
    auto carin = rm::Person{"Carin"s, 42};
    auto doris = rm::Person{"Doris"s, 52};
    cout << "[use_case_1] #persons: " << rm::Person::instances() << "\n";
    for (auto&& p: {anna, berit, carin, doris}) cout << p << "\n";
}

void use_case_2() {
    auto const N = 1'000'000UL;
    auto vec = std::vector<rm::Person>{};
    vec.reserve(N);
    for (auto k = 0UL; k < N; ++k) {
        vec.emplace_back("person-"s + std::to_string(k + 1), k + 20 % 100);
    }
    cout << "[use_case_2] #persons: " << rm::Person::instances() << "\n";
}

int main() {
    cout << "[main] (first) #persons: " << rm::Person::instances() << "\n";
    use_case_1();
    cout << "[main] (middle) #persons: " << rm::Person::instances() << "\n";
    use_case_2();
    cout << "[main] (last)  #persons: " << rm::Person::instances() << "\n";
}

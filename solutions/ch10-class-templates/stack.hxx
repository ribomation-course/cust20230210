#pragma once

#include <stdexcept>

template<typename TYPE, unsigned CAPACITY>
class Stack {
    TYPE stk[CAPACITY]{};
    int  top = 0;

public:
    unsigned size() const { return top; }
    unsigned capacity() const { return CAPACITY; }
    bool empty() const { return size() == 0; }
    bool full() const { return size() == capacity(); }

    void push(TYPE x) {
        if (full()) throw std::overflow_error{"stack full"};
        stk[top++] = x;
    }

    TYPE pop() {
        if (empty()) throw std::underflow_error{"stack empty"};
        return stk[--top];
    }
};
